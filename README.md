#hanami-webpack

:warning: **This is a very initial version, basically a PoC, but it works, so, be aware!**

This plugin will help you to use [Webpack](webpack.github.io) as your asset pipeline for Hanami with [webpack-dev-server](http://webpack.github.io/docs/webpack-dev-server.html)  for development.

It'll works without any problem with your existent assets using the [hanami/assets](https://github.com/hanami/assets).

##Setup

I'll assume that you already have the Node.js already installed and you will start (or already done) a Webpack config.

1. Add the `gem 'hanami-webpack'` on your Gemfile.
2. Run `bundle install`.
3. Add the xxx plugin on your Webpack:

```
new StatsPlugin("manifest.json", {
  chunkModules: false,
  source: false,
  chunks: false,
  modules: false,
  assets: true
})
```

##Usage

On development this plugin will try to start the **[webpack-dev-server](http://webpack.github.io/docs/webpack-dev-server.html)** with your Hanami server. You can disable this behavior (see [configuration section](#configuration)).

You will need use the `webpack_asset_path` helper on your templates to get the correct bundle path.

Let's say that you have a [bundle](http://webpack.github.io/docs/configuration.html#entry) with the name `web.people`. You should place on your template:

```erb
<script src="<%= webpack_asset_path('web.people') %>"></script>
```

To make a build just run `./node_modules/.bin/webpack` on the project root.

##Configuration

All plugin configuration is done by the following ENV vars:

| Name | Default Value | Description |
| --- | --- | --- |
| `WEBPACK_DEV_SERVER_HOST` | `localhost` | The host where your asset dev server is running. |
| `WEBPACK_DEV_SERVER_PORT` | `3020` | The port where your asset dev server is running. |
| `WEBPACK_PUBLIC_PATH` | `/` | Whenever you want use other [publicPath](http://webpack.github.io/docs/configuration.html#output-publicpath) you should update this var. |
| `INBUILT_WEBPACK_DEV_SERVER` | `true` | If you want start the webpack-dev-server when you start your hanami server (except on production). |
| `WEBPACK_DEV_SERVER` | `false` on production `true` in any other env. | If you want disable the webpack-dev-server integration set this as `false`. |

##With Heroku
Just run this commands to configure your Heroku server. This commands will set your Hanami to serve the assets (you probably already done this) and will set the Ruby and Node.js buildpacks.

```
$ heroku config:set SERVE_STATIC_ASSETS=true
$ heroku buildpacks:set --index 1 heroku/nodejs
$ heroku buildpacks:set --index 2 heroku/ruby
```

To build your assets when you deploy your app you will need place on your `package.json` the [Heroku auto-commands](https://devcenter.heroku.com/articles/nodejs-support#heroku-specific-build-steps):

```
{
  ...
  "scripts": {
    "build": "webpack",
    "heroku-postbuild": "npm run build"
  }
}
```

## Todo

* Write tests.
* Research a better way to do these things without monkey patches (I already done, but looks like that with the current Hanami version it's impossible).
* I want to create a rake task to create a ready to go Webpack config, but actually looks like that it's impossible.

----------
Samuel Simões ~ [@samuelsimoes](https://twitter.com/samuelsimoes) ~ [Blog](http://blog.samuelsimoes.com/)